#! /usr/bin/perl -w

use Getopt::Long;
use Pod::Usage;
use File::Find;
use DBI;

my $help = 0;
my $usage = 0;
my $verbose = 0;
my $path = '';
my $zero = 0;
my $home = $ENV{'HOME'};

sub main() {
    checkargs();
    if (!$path) {
        $path = $home . '/.xbmc/userdata/Database';
    }
    print("# checking XBMC directory '$path'\n") if ($verbose);
    $dbpath = finddb($path);
    if (!$dbpath) {
        pod2usage("$0: can't find a XBMC database in '$path'.");
    }
    print("# using database '$dbpath'\n") if ($verbose);
    checkdb();
}

# list videos database, find the latest one
# modified version of
# http://stackoverflow.com/questions/4651092/getting-the-list-of-files-sorted-by-modification-date-in-perl
sub finddb($) {
    my $path = shift(@_);
    opendir my($dirh), $path or die "can't opendir $path: $!";
    my @flist = sort {  -M $a <=> -M $b } # Sort by modification time
        map  { "$path/$_" } # We need full paths for sorting
        grep { /^MyVideos.*\.db$/ }
        readdir $dirh;
    closedir $dirh;
    if ($#flist > 0) {
        return $flist[0];
    }
    else {
        return 0;
    }
}

sub checkargs() {
    GetOptions('h|?' => \$help,
               'help|usage' => \$usage,
               'path=s' => \$path,
               'home=s' => \$home,
               'zero|0' => \$zero,
               'verbose|v' => \$verbose,
        )
        or die("Error parsing commandline\n");

    pod2usage(1) if $help;
    pod2usage(-exitval => 0, -verbose => 2) if $usage;

    if ($#ARGV < 0) {
        die("need path to media library (e.g. /home/media/video/films)\n");
    }
}

sub find_xbmc_file() {
    -d $_ && return; 
    /\.(mp3|mds|sfv|nfo|txt|srt|sub|idx|rar|jpeg|jpg|dvdrip-info|png|url|lnk|smi|vob|bup)$/i && return;
    #/screenshot/i && return;
    /VTS_\d+_\d+.IFO/i && return;
    /sample/i && return;
    /\/sample\./i && return;
    /\/(sample|subs|subtitles)\//i && return;
    chomp;
    my $sql = qq(SELECT (path.strPath || files.strFileName) AS p FROM
    movie JOIN files ON files.idFile=movie.idFile JOIN path ON
    path.idPath=files.idPath WHERE p LIKE ?;');
    my $sth = $dbh->prepare($sql);
    $sth->execute("%" . $_ . "%") or die $DBI::errstr;
    $sth->fetchall_arrayref; # to make -> rows work
    # XXX: maybe we should just dump the DB in a hash and look those up
    # instead of repeating N SQL queries... time vs memory?
    my $rv = $sth->rows;
    if ($rv <= 0)  {
        print "not found: " if $verbose;
        print "$_";
        if ($zero) {
            print "\0";
        }
        else {
            print "\n";
        }
    }
    else {
        print "FOUND: $_\n" if $verbose;
    }
}

sub checkdb() {
    $dbh = DBI->connect("dbi:SQLite:dbname=$dbpath", "", "");
    find({ wanted => \&find_xbmc_file, no_chdir => 1}, $ARGV[0]);
}

main();

__END__
=encoding utf8

=head1 NAME

xbmc-orphans - find files missing from XBMC

=head1 SYNOPSIS

xbmc-orphans [--path .xbmc/userdata/Database] <dir>

 Options:
  -h         short usage
  --help     complete help
  --home     the home directory where the .xbmc directory is located
  --path     the location of the Database directory of XBMC, overrides --home
  --verbose  show interaction details with the database

<dir> is a directory to look into to find files that may be missing from
the library.

=head1 DESCRIPTION

This program will look into the XBMC database for the "playcount"
field to register that number as metadata in the git-annex repository.

=head1 OPTIONS

=over 8

=item B<--home>

Home of the user running XBMC. If not specified, defaults to the $HOME
environment variables. The script will look into
B<$home/.xbmc/userdata/Database> for a file matching
B<^MyVideos.*\.db$> and will fail if none is found.

=item B<--path>

Manually specify the path to B<.xbmc/userdata/Database>. This
overrides B<--home>.

Note that this doesn't point directly to the database itself, because
there are usually many database files and we want to automatically
find the latest. This may be a stupid limitation.

=item B<--verbose>

Show more information about path discovery.

=back

=head1 ENVIRONMENT

B<$HOME> is looked into to find the B<.xbmc> home directory if none of
B<--home> or B<--path> is specified.

=head1 FILES

=over 8

=item B<$HOME/.xbmc/userdata/Database/MyVideos.*\.db>

This is where we assume the SQLite database of videos XBMC uses is
stored.

=back

=head1 LIMITATIONS

This doesn't support Kodi by default, but using the right --path fixes
that.

The script is not incremental, so it will repeatedly add the same
counts to files it has already found.

False negatives: we may silently skip some files that match some of our
ignore pattern - we should at least be more noisy when skipping under
verbose.

False positives: still needs to be completely audited to see if the
files reported are really missing, but should be pretty complete for
"stacked" items and single ones.

Could sort the output by latest modified last, maybe through -tr like
ls. For now, you can use "xbmc-orphans <dir> -0 | xargs -0 ls -tr" for a
similar result. This is clumsy because we are already doing a stat on
the file...

=head1 SEE ALSO

B<xbmc>(1)

Was originally forked from git-annex-xbmc-playcount.pl.

=head1 AUTHOR

Written by Antoine Beaupré <anarcat@debian.org>
