#!/usr/bin/python3

import argparse
import logging

import requests


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized befure this, using
    `basicConfig`.
    """

    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)


def main():
    logging.basicConfig(format='%(levelname)s: %(message)s')
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', default='https://gitlab.torproject.org/')
    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    args = parser.parse_args()

    page = 1
    session = requests.Session()
    page_count_total = 0
    while True:
        project_list = session.get(f'{args.url}api/v4/projects?per_page=100&page={page}')
        if not len(project_list.json()):
            break
        for project in project_list.json():
            id = project['id']
            path = project['path_with_namespace']
            wiki_pages = session.get(f'{args.url}api/v4/projects/{id}/wikis')
            if wiki_pages.status_code == requests.codes.ok:
                wiki_path = f'{args.url}{path}/-/wikis/'
                page_count = len(wiki_pages.json())
                page_count_total += page_count
                if page_count > 0:
                    logging.info("%d pages in %s", page_count, wiki_path)
                    print(wiki_path)
                else:
                    logging.info('empty wiki at %s', wiki_path)
        page += 1
    logging.info("total page count: %d", page_count_total)

if __name__ == '__main__':
    main()
