package main

// read a huge file mapping of "foo HASH" and count the number of
// duplicate HASH entries

// this fails. it takes up 14GB of RAM here which is pretty much all
// my machine has to offer, after 43% of a 21GB file

// not sure how this can be optimized better - we really store only
// what we can...
import (
	"bufio"
	"encoding/hex"
	"fmt"
	"log"
	"math"
	"os"
	"strings"

	"gopkg.in/cheggaaa/pb.v1"
)

func main() {
	hashfile := "hashes"
	info, err := os.Stat(hashfile)
	if err != nil {
		log.Fatal("hashfile missing: ", err)
	}
	bar := pb.StartNew(int(info.Size()))
	// 10-digit hash = 40 bits (32 < hash < 64), and "seen" flag
	m := make(map[uint64]bool)
	file, err := os.Open(hashfile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	colls := 0
	lines := 0

	// mess with the terminal format to show the collisions
	colsWidth := 1
	width, err := pb.GetTerminalWidth()
	bar.SetWidth(width - colsWidth)
	// equivalent to format := "\r%1.d%s"
	// but designed to change as the number goes up
	// XXX: we could also just trust that we do the right thing here...
	format := fmt.Sprintf("\r%%%d.d%%s", colsWidth)

	// use a callback instead of letting the progress bar print so we
	// add the number of collisions so far
	bar.Callback = func(out string) {
		fmt.Printf(format, colls, out)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		h := strings.SplitN(line, "\t", 2)
		decoded, err := hex.DecodeString(h[1])
		if err != nil {
			log.Fatal(err)
		}
		// this fails, presumably because the size is too small?
		//hash := binary.BigEndian.Uint64(decoded)
		// instead we just iterate over every entry
		hash := uint64(0)
		for _, b := range decoded {
			hash = (hash << 8) | uint64(b)
		}
		if m[hash] {
			colls += 1
			// check if our number is wider, in base 10 of course
			pow := int(math.Pow(10, float64(colsWidth)))
			if colls%pow == 0 {
				colsWidth += 1
				format = fmt.Sprintf("\r%%%d.d%%s", colsWidth)
				bar.SetWidth(width - colsWidth)
			}
		}
		m[hash] = true
		lines += 1
		bar.Add(len(line))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	bar.Finish()
	log.Printf("%d collisions found (%.4f%%)", colls, colls/lines)
}
