#!/usr/bin/python3

"""focus the next or previous window in sway or i3, but across all
containers in the current workspace"""

# performance-wise, this is slow, but not *that* slow. it's about 30
# miliseconds, which doesn't feel instant. it's odd because runnigq

import argparse
from functools import lru_cache
import json
import logging
from os import environ
import subprocess
import sys
from typing import Literal, Optional


class Messenger():
    def __init__(self, command: Literal['swaymsg', 'i3-msg']):
        self.command = command

    @lru_cache(maxsize=0)
    def get_tree(self) -> dict:
        msg = subprocess.run((self.command, '-t', 'get_tree'), stdout=subprocess.PIPE, check=True)
        return json.loads(msg.stdout)

    def all_nodes(self, root: Optional[dict] = None):
        if root is None:
            root = self.get_tree()
        for node in root['nodes'] + root['floating_nodes']:
            yield node
            if node.get('nodes') or node.get('floating_nodes'):
                for n in self.all_nodes(node):
                    yield n

    def nodes_per_workspace(self) -> dict[str, list[dict]]:
        workspaces: dict[str, list[dict]] = {}
        current = None
        for node in self.all_nodes():
            if node.get('type') == 'workspace':
                current = node.get('name')
                logging.debug('found workspace %s', current)
                if not workspaces.get(current):
                    workspaces[current] = []
            elif node.get('type') in ('con', 'floating_con') and node.get('name'):
                logging.debug("found con: %s", node.get('name'))
                assert current, "oops, not in a workspace?"
                workspaces[current].append(node)
            else:
                logging.debug("found %s [unhandled]: %s", node.get('type'), node.get('name'))
        return workspaces

    def nodes_in_workspace(self, ws) -> list[dict]:
        return self.nodes_per_workspace()[ws.get('name')]


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized befure this, using
    `basicConfig`.
    """

    def __init__(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):  # type: ignore[no-untyped-def]
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


def main():
    parser = argparse.ArgumentParser(epilog=__doc__)
    parser.add_argument('direction', choices=('prev', 'next'))
    logging.basicConfig(level="WARNING", format="%(message)s")
    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    if (environ.get('WAYLAND') or environ.get('XDG_CURRENT_DESKTOP') == 'sway'):
        default_command = 'swaymsg'
    else:
        default_command = 'i3-msg'
    parser.add_argument(
        "--command",
        default=default_command,
        choices=('i3-msg', 'swaymsg'),
        help="which messaging backend to use, one of %(choices)s, default: %(default)s",
    )
    args = parser.parse_args()
    ms = Messenger(args.command)
    to_focus = None
    for ws, nodes in ms.nodes_per_workspace().items():
        for i in range(len(nodes)):
            if to_focus is None:
                # set a fallback node
                to_focus = nodes[i]
            if nodes[i].get("focused"):
                logging.info("window %s focused", nodes[i].get('name'))
                if i+1 >= len(nodes):
                    to_focus = nodes[0]
                else:
                    to_focus = nodes[i+1]
                logging.info("found window %s to focus", to_focus.get('name'))
                break
            else:
                logging.debug("window %s not focused", nodes[i].get('name'))
    if not to_focus:
        logging.error("no window found")
        sys.exit(1)
    search = "[con_id=%s]" % to_focus["id"]
    command = (args.command, search, 'focus')
    logging.debug("running %s", command)
    sway = subprocess.run(command, stdout=subprocess.PIPE)
    sys.exit(sway.returncode)


if __name__ == '__main__':
    main()
