#!/bin/sh

# ridiculous scraper for radio-canada audio shows
#
# many limitations:
# - hardcodes URL
# - probably brittle
# - untested on other shows, let alone videos and especially not OHdio
# - doesn't add proper tags to the audio file (artist, album, etc)
# - should be a yt-dlp extractor instead, see https://github.com/yt-dlp/yt-dlp/issues/6678

set -e

#set -x

BaseURL='https://ici.radio-canada.ca/jeunesse/scolaire/emissions/5615/lagent-jean/contenu/audios'

curl -s "$BaseURL" | sed -n '/medianet-content/{s/.*href="/https:\/\/ici.radio-canada.ca/;s/".*//;p}' | while read EpUrl; do

    #EpUrl='https://ici.radio-canada.ca/jeunesse/scolaire/emissions/5615/lagent-jean/episodes/456610/neo-agence-cheffe-sauver-monde-ennemie/7228/audios'
    # look for a <script .*type=consolevideo+json>, it's our magic JSON blob
    #IdMedia=$(curl -s "$EpUrl" | grep -Po '"IdMedia":"\d+"' | grep -Po '\d+')
    JSON_BLOB=$(curl -s "$EpUrl" | sed -n '/<script[^>]*application[^>]*>/,/<\/script>/p' | sed -e '/<\/\?script/d')

    IdMedia=$(
        jq -r .Media.IdMedia <<EOF
$JSON_BLOB
EOF
           )
    Title=$(
        jq -r .Title <<EOF
$JSON_BLOB
EOF
         )
    m3u_url=$(curl -s 'https://services.radio-canada.ca/media/validation/v2/?appCode=medianet&connectionType=hd&deviceType=ipad&idMedia='"$IdMedia"'&multibitrate=true&output=json&tech=hls' | jq -r .url)
    yt-dlp -o "$Title".mp4 "$m3u_url"
done
