#!/usr/bin/python3

'''Check which packages are in ELPA and Debian by looking up packages
on ELPA, the local apt-cache or using wnpp-check (which works
online).
'''

import argparse
import distutils.version
import json
import logging
import subprocess

import requests

DEBIAN_PKG_URL = 'https://tracker.debian.org/%s'
DEBIAN_PKG_SEARCH = 'https://api.ftp-master.debian.org/madison?f=json&package=%s'
#DEBIAN_PKG_SEARCH = 'https://qa.debian.org/madison.php?table=all&text=on&package=%s'
DEBIAN_NEW_SEARCH = 'https://ftp-master.debian.org/new/%s_%s.html'

# convenience copy, use the actual function instead, as levels may be
# added after this module is loaded
levels = logging._nameToLevel.keys()


def logging_args(parser):
    """
    >>> parser = argparse.ArgumentParser()
    >>> logging_args(parser)
    >>> parser.parse_args(['--verbose'])
    Namespace(email=None, logfile=None, loglevel='INFO')
    >>> parser.parse_args(['--verbose', '--debug'])
    Namespace(email=None, logfile=None, loglevel='DEBUG')
    """
    default_level = 'WARNING'
    parser.add_argument('-v', '--verbose',
                        dest='loglevel', action='store_const',
                        const='INFO', default=default_level,
                        help='enable verbose messages')
    parser.add_argument('-d', '--debug',
                        dest='loglevel', action='store_const',
                        const='DEBUG', default=default_level,
                        help='enable debugging messages')
    parser.add_argument('--loglevel', choices=logging._nameToLevel.keys(),
                        default=default_level, type=str.upper,
                        help='expliticly set logging level')


def find_debian_pkg_apt_cache(pkg):
    cmd = ['apt-cache', 'search', '--names-only', pkg]
    output = subprocess.check_output(cmd).decode('utf-8')
    if not output:
        # apt-cache does not raise a proper error code on failures
        return None, None
    debian_pkg, debian_desc = output.split(' - ')
    if pkg != debian_pkg:
        logging.warning('close match found: %s', debian_pkg)
    debian_url = DEBIAN_PKG_SEARCH % debian_pkg
    return debian_pkg, debian_url


def find_debian_pkg_rmadison(packages, pkg):
    if len(packages) < 1 or pkg not in packages[0]:
        return None
    packages = { suite: list(vers.keys()) for suite, vers in packages[0][pkg].items() }
    # highest version in Debian
    versions = [ distutils.version.LooseVersion(vers)
                 for suite in packages for vers in packages[suite] ]
    if versions:
        version = str(max(versions))
    else:
        version = ''
    # only in NEW?
    if 'new' in packages and len(packages) == 1:
        return { 'debian_pkg': 'yes',
                 'debian_url': DEBIAN_NEW_SEARCH % (pkg, version[0]),
                 'debian_version': 'NEW: %s' % version,
        }
    elif len(packages) >= 1:
        return { 'debian_pkg': 'yes',
                 'debian_url': DEBIAN_PKG_URL % pkg,
                 'debian_version': version,
        }
    return None


find_debian_pkg = find_debian_pkg_rmadison


def find_debian_wnpp(pkg):
    try:
        subprocess.check_output(['wnpp-check', pkg])
    except subprocess.CalledProcessError as e:
        # wnpp-check returns codes are backwards
        bugnum, url, _ = e.output.decode('utf-8').split("\n")[0].split()[2:]
        return { 'debian_pkg': None,
                 'debian_url': url,
                 'debian_version': bugnum.rstrip(')'),
        }
    else:
        return None


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    logging_args(parser)
    parser.add_argument('packages', nargs='+', help='Emacs packages')
    parser.add_argument('--format', choices=['markdown', 'json'], default='markdown',
                        help='output format')
    parser.add_argument('--elpa-base-url', default='https://melpa.org/',
                        help='base URL for the ELPA archive (default: %(default)s)')
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel, format='%(message)s')

    ELPA_ARCHIVE_URL = args.elpa_base_url + 'archive.json'
    ELPA_SEARCH_URL = args.elpa_base_url + '#/%s'

    logging.info('fetching ELPA database from %s', ELPA_ARCHIVE_URL)
    elpa_archive = requests.get(ELPA_ARCHIVE_URL).json()

    url = DEBIAN_PKG_SEARCH % " ".join(map(lambda x: 'elpa-' + x, args.packages))
    logging.info('fetching Debian package versions from %s', url)
    debian_packages = requests.get(url).json()

    if args.format == 'markdown':
        print('''Package | Emacs | Debian | Description
------- | ----- | ------ | -----------''')

    dump = {}
    for pkg in args.packages:
        metadata = {'pkg': pkg,
                    'desc': None,
                    'debian_pkg': None,
                    'debian_url': 'https://bugs.debian.org/',
                    'debian_version': None,
                    'elpa_pkg': None,
                    'elpa_version': None,
                    'elpa_url': 'https://github.com/melpa/melpa/blob/master/CONTRIBUTING.org',
        }
        logging.info('processing package %s', pkg)
        if elpa_archive.get(pkg):
            logging.info('package found in ELPA')
            metadata['desc'] = elpa_archive[pkg]['desc'].strip()
            version = ".".join(map(str, elpa_archive[pkg].get('ver', [])))
            metadata['elpa_pkg'] = pkg
            metadata['elpa_version'] = version
            metadata['elpa_url'] = ELPA_SEARCH_URL % pkg
        else:
            logging.warning('package %s not found in ELPA', pkg)
        logging.info('checking in Debian')
        meta = find_debian_pkg(debian_packages, 'elpa-' + pkg)
        if meta:
            metadata.update(meta)
        else:
            logging.info('not found, checking for WNPP entry')
            meta = find_debian_wnpp(pkg)
            if meta:
                metadata.update(meta)
            else:
                logging.warning('package %s not found in Debian', pkg)
        if args.format == 'markdown':
            print("{pkg} | [{elpa_version}]({elpa_url}) | [{debian_version}]({debian_url}) | {desc}".format(**metadata))
        else:
            dump[pkg] = metadata
    if args.format == 'json':
        print(json.dumps(dump))


if __name__ == '__main__':
    main()
